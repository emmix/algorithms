
#include <iostream>


void insertionSort(int a[], int n)
{
	for (int j=1; j<n; j++) {
		int key = a[j];
		int i = j-1;
		while (i>-1 && a[i]>key) {
			a[i+1] = a[i];
			i--;
		}
		i++;
		a[i] = key;
	}
}

int main()
{
	int a[] = {4, 5, 6, 1};
	insertionSort(a, 4);
	std::cout<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<std::endl;
}
