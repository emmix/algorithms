#include <iostream>


int prices[] = {10,11, 7, 10, 6};


void maxProfit(int prices[], int size, int &buy, int &sell) {
        int maxProfit = 0;
        int profit;
        buy = 0;
        sell = 0;
        for(int i=0; i<size; i++) {
                for(int j=i+1; j<size; j++) {
                        profit = prices[j] - prices[i];
                        if (profit>maxProfit) {
                                maxProfit = profit;
                                buy = i;
                                sell = j;
                        }
                }
        }
}

int main()
{
        int buy;
        int sell;
        maxProfit(prices, 5, buy, sell);

        std::cout<<buy<<" "<<sell<<" "<<prices[sell]-prices[buy]<<std::endl;

}
