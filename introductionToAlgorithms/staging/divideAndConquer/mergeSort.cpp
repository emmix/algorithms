
#include <cassert>
#include <iostream>

// [p, q] and  [q+1, r]
void MERGE1(int A[], int p, int q, int r)
{
        for (int i=q+1; i<=r; i++) {
                int key = A[i];

                for (int j=i-1; j>=p; j--) {
                        if (key >= A[j]) {
                                j++;
                                A[j] = key;
                                break;
                        } else {
                                A[j+1] = A[j];
                        }
                }
        }
}

void MERGE2(int A[], int p, int q, int r)
{
        int n1 = q - p + 1;
        int n2 = r-(q+1) +1;
        int L[n1];
        int R[n2];

        // init L
        for (int i=0; i<n1; i++) {
                L[i] = A[p+i];
        }
        // init R
        for (int i=0; i<n2; i++) {
                R[i] = A[q+1+i];
        }

        int i = 0;
        int j = 0;
        for (int k=p; k<=r; k++) {

                if (i<n1) { // i<n1 
                        if(j<n2) {
                                if (L[i]<=R[j]) {
                                        A[k] = L[i];
                                        i++;
                                } else {
                                        A[k] = R[j];
                                        j++;
                                }

                        } else { //j>=n2
                                A[k] = L[i];
                                i++;
                        }
                } else { // i>=n1
                        if(j<n2) {
                                A[k] = R[j];
                                j++;
                        } else { //j>=n2

                        }
                }

        }
}

#define  SENTINEL  (0x7fffffff)
void MERGE(int A[], int p, int q, int r)
{
        int n1 = q - p + 1;
        int n2 = r-(q+1) +1;
        int L[n1 + 1];
        int R[n2 + 1];

        // init L
        for (int i=0; i<n1; i++) {
                L[i] = A[p+i];
        }
        L[n1] = SENTINEL;
        // init R
        for (int i=0; i<n2; i++) {
                R[i] = A[q+1+i];
        }
        R[n2] = SENTINEL;

        int i = 0;
        int j = 0;
        for (int k=p; k<=r; k++) {
                if (L[i]<=R[j]) {
                        A[k] = L[i];
                        i++;
                } else {
                        A[k] = R[j];
                        j++;
                }
        }
}


void MERGE_SORT(int A[], int p, int q)
{
        assert(p<=q);

        if (p==q) {
                return;
        }

        int m = p + (q-p)/2;
        MERGE_SORT(A, p, m);
        MERGE_SORT(A, m+1, q);

        MERGE2(A, p, m, q);
}


int main()
{
        int A[] = {1, 4, 2, 3, 5};

        std::cout<<A[0]<<" "<<A[1]<<" "<<A[2]<<" "<<A[3]<<" "<<A[4]<<std::endl;
        MERGE_SORT(A, 0, 4);
        std::cout<<A[0]<<" "<<A[1]<<" "<<A[2]<<" "<<A[3]<<" "<<A[4]<<std::endl;

        return 0;
}
