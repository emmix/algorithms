#include <vector>
#include <cassert>
#include <iostream>

using Matrix = std::vector<std::vector<double>>;

inline std::ostream& operator<< (std::ostream &os, const Matrix &M) {

        int r = M.size();
        assert(r>0);
        int c = M[0].size();;
        assert(c>0);

        for(int i=0; i<r; i++) {
                for(int j=0; j<c; j++) {
                       std::cout<<M[i][j]<<" ";
                }
                std::cout<<std::endl;
        }

        return os;
}

Matrix MATRIX_MULTIPLY(Matrix A, Matrix B)
{
        int ar = A.size();
        int ac = A[0].size();;

        int br = B.size();
        int bc = B[0].size();

        assert(ac == br);
        int acbr = ac;


        Matrix C(ar, std::vector<double>(bc,0));

        for (int i=0; i<ar; i++)
        {
                for(int j=0; j<bc; j++)
                {
                        for(int k=0; k<acbr; k++) {
                                C[i][j] += A[i][k]*B[k][j];

                        }
                }
        }

        return C;
}

int main()
{
        Matrix A = {
                {1, 1, 2},
                {1, 2, 0},
                {2, 1, 1},
        };
        Matrix B = {
                {1, 1, 2},
                {1, 2, 0},
                {2, 1, 1},
        };

        Matrix C = MATRIX_MULTIPLY(A, B); 
        std::cout<<C<<std::endl;
}
