
#pragma once

const int t = 2;
struct Node {
	bool leaf;
	int n;
	Node* c[2*t];
	char key[2*t-1];
};

struct BTree {
	Node *root;
};

