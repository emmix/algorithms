

# Textbook
http://mitpress.mit.edu/books/introduction-algorithms-third-edition
https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-introduction-to-algorithms-sma-5503-fall-2005/

# solutions
Intro_to_Algo_Selected_Solutions.pdf
https://walkccc.github.io/CLRS/Chap06/6.1/
https://ita.skanev.com/index.html

# references
http://web.cecs.pdx.edu/~maier/cs584/

